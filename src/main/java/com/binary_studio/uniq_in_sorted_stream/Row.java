package com.binary_studio.uniq_in_sorted_stream;

import java.util.Objects;

public final class Row<RowData> {

	private RowData id;

	public Row(RowData id) {
		this.id = id;
	}

	public RowData getPrimaryId() {
		return this.id;
	}

	public void setPrimaryId(RowData id) {
		this.id = id;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Row<?> row = (Row<?>) o;
		return Objects.equals(this.id, row.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.id);
	}
}
