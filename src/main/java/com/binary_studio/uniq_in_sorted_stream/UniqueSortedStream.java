package com.binary_studio.uniq_in_sorted_stream;

import java.util.function.Predicate;
import java.util.stream.Stream;

public final class UniqueSortedStream {

	private UniqueSortedStream() {
	}

	public static <T> Stream<Row<T>> uniqueRowsSortedByPK(Stream<Row<T>> stream) {
		return stream.filter(getFilterFunction());
	}

	private static <T> Predicate<Row<T>> getFilterFunction() {
		Row<T> currentRow = new Row<>(null);
		return row -> {
			if (currentRow.getPrimaryId() == null) {
				currentRow.setPrimaryId(row.getPrimaryId());
				return true;
			}
			else if (row.equals(currentRow)) {
				return false;
			}
			else {
				currentRow.setPrimaryId(row.getPrimaryId());
				return true;
			}
		};
	}

}
