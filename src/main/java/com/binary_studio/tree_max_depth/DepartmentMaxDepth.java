package com.binary_studio.tree_max_depth;

import java.util.ArrayList;
import java.util.List;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	private static int calculateDepth(List<Department> nodes) {
		int depth = 0;
		List<Department> newNodes;
		while (nodes.size() > 0) {
			newNodes = new ArrayList<>();
			for (Department department: nodes) {
				for (Department subDepartment: department.getSubDepartments()) {
					if (subDepartment != null) {
						newNodes.add(subDepartment);
					}
				}
			}
			depth += 1;
			nodes = newNodes;
		}
		return depth;
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		if (rootDepartment == null) {
			return 0;
		}
		ArrayList<Department> startNodes = new ArrayList<Department>();
		startNodes.add(rootDepartment);
		return calculateDepth(startNodes);
	}

}
