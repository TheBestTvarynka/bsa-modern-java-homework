package com.binary_studio.fleet_commander.core.ship;

import java.awt.*;
import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class CombatReadyShip implements CombatReadyVessel {

	private final PositiveInteger startCapacitorAmount;

	private final PositiveInteger startShieldHP;

	private final PositiveInteger startHullHP;

	private String name;

	private PositiveInteger shieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger powergridOutput;

	private PositiveInteger capacitorAmount;

	private PositiveInteger capacitorRechargeRate;

	private PositiveInteger speed;

	private PositiveInteger size;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	public CombatReadyShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size, AttackSubsystem attackSubsystem,
			DefenciveSubsystem defenciveSubsystem) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.startShieldHP = shieldHP;
		this.hullHP = hullHP;
		this.startHullHP = hullHP;
		this.powergridOutput = powergridOutput;
		this.capacitorAmount = capacitorAmount;
		this.startCapacitorAmount = capacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.speed = speed;
		this.size = size;
		this.attackSubsystem = attackSubsystem;
		this.defenciveSubsystem = defenciveSubsystem;
	}

	@Override
	public void endTurn() {
		// TODO: regenerate capacity
		int capacity = this.capacitorAmount.value() + this.capacitorRechargeRate.value();
		this.capacitorAmount = PositiveInteger
				.of(this.startCapacitorAmount.value() < capacity ? this.startCapacitorAmount.value() : capacity);
	}

	@Override
	public void startTurn() {
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PositiveInteger getSize() {
		return this.size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.speed;
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		int capacitorConsumption = this.attackSubsystem.getCapacitorConsumption().value();
		if (this.capacitorAmount.value() < capacitorConsumption) {
			return Optional.empty();
		}
		this.capacitorAmount = PositiveInteger.of(this.capacitorAmount.value() - capacitorConsumption);
		return Optional.of(new AttackAction(this.attackSubsystem.attack(target), this, target, this.attackSubsystem));
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		AttackAction reducedAttack = this.defenciveSubsystem.reduceDamage(attack);
		int damage = reducedAttack.damage.value();
		if (this.shieldHP.value() + this.hullHP.value() <= damage) {
			this.shieldHP = PositiveInteger.of(0);
			this.hullHP = PositiveInteger.of(0);
			return new AttackResult.Destroyed();
		}
		int restHP = this.hullHP.value() + this.shieldHP.value() - damage;
		if (restHP > this.hullHP.value()) {
			this.shieldHP = PositiveInteger.of(this.hullHP.value() - damage);
		}
		else {
			this.shieldHP = PositiveInteger.of(0);
			this.hullHP = PositiveInteger.of(restHP);
		}
		return new AttackResult.DamageRecived(reducedAttack.weapon, PositiveInteger.of(damage), reducedAttack.target);
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		int capacitorConsumption = this.defenciveSubsystem.getCapacitorConsumption().value();
		if (capacitorConsumption > this.capacitorAmount.value()) {
			return Optional.empty();
		}
		RegenerateAction regen = this.defenciveSubsystem.regenerate();
		this.capacitorAmount = PositiveInteger.of(this.capacitorAmount.value() - capacitorConsumption);

		int newShieldHP = this.shieldHP.value() + regen.shieldHPRegenerated.value();
		int resultShieldHP = this.startShieldHP.value() < newShieldHP ? this.startShieldHP.value() : newShieldHP;

		int newHullHP = this.hullHP.value() + regen.hullHPRegenerated.value();
		int resultHullHP = this.startHullHP.value() < newHullHP ? this.startHullHP.value() : newHullHP;

		RegenerateAction resultAction = new RegenerateAction(PositiveInteger.of(resultShieldHP - this.shieldHP.value()),
				PositiveInteger.of(resultHullHP - this.hullHP.value()));

		this.shieldHP = PositiveInteger.of(resultShieldHP);
		this.hullHP = PositiveInteger.of(resultHullHP);

		return Optional.of(resultAction);
	}

}
