package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ModularVessel {

	private String name;

	private PositiveInteger shieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger powergridOutput;

	private PositiveInteger capacitorAmount;

	private PositiveInteger capacitorRechargeRate;

	private PositiveInteger speed;

	private PositiveInteger size;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	public DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger powergridOutput,
			PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate, PositiveInteger speed,
			PositiveInteger size) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.powergridOutput = powergridOutput;
		this.capacitorAmount = capacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.speed = speed;
		this.size = size;
	}

	public String getName() {
		return this.name;
	}

	public PositiveInteger getSpeed() {
		return this.speed;
	}

	public PositiveInteger getSize() {
		return this.size;
	}

	public PositiveInteger getCapacitorAmount() {
		return this.capacitorAmount;
	}

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {
		return new DockedShip(name, shieldHP, hullHP, powergridOutput, capacitorAmount, capacitorRechargeRate, speed,
				size);
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			this.attackSubsystem = null;
			return;
		}
		int pg = this.powergridOutput.value();
		int attackSubsystemPG = subsystem.getPowerGridConsumption().value();
		int defenciveSubsystemPG = this.defenciveSubsystem == null ? 0
				: this.defenciveSubsystem.getPowerGridConsumption().value();
		if (attackSubsystemPG + defenciveSubsystemPG > pg) {
			throw new InsufficientPowergridException(attackSubsystemPG + defenciveSubsystemPG - pg);
		}
		this.attackSubsystem = subsystem;
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			this.defenciveSubsystem = null;
			return;
		}
		int pg = this.powergridOutput.value();
		int attackSubsystemPG = this.attackSubsystem == null ? 0 : subsystem.getPowerGridConsumption().value();
		int defenciveSubsystemPG = subsystem.getPowerGridConsumption().value();
		if (attackSubsystemPG + defenciveSubsystemPG > pg) {
			throw new InsufficientPowergridException(attackSubsystemPG + defenciveSubsystemPG - pg);
		}
		this.defenciveSubsystem = subsystem;
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		if (this.attackSubsystem == null && this.defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.bothMissing();
		}
		if (this.attackSubsystem == null) {
			throw NotAllSubsystemsFitted.attackMissing();
		}
		if (this.defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.defenciveMissing();
		}
		return new CombatReadyShip(this.name, this.shieldHP, this.hullHP, this.powergridOutput, this.capacitorAmount,
				this.capacitorRechargeRate, this.speed, this.size, this.attackSubsystem, this.defenciveSubsystem);
	}

}
